<?php

return _document(
    _html(
        _head(
            _block(
                'head',
                fn() => _wrapper(
                    _title(
                        _block('title', fn() => _text('Some Title'))
                    )
                )
            )
        ),
        _body(
            _block('body', fn() => _text('Hello World'))
        )
    )
);