<?php


namespace NoTee;


use PHPUnit\Framework\TestCase;

class ExtensibilityTest extends TestCase
{
    public function test()
    {
        global $noTee;
        $noTee = new NodeFactory(new DefaultEscaper('utf-8'), new UriValidator(), new BlockManager());
        require_once __DIR__ . '/../global.php';
        $templateDirs = [
            __DIR__ . '/example_project/base',
            __DIR__ . '/example_project/first_child',
            __DIR__ . '/example_project/last_child',
        ];
        $template = new Template($templateDirs, $noTee);
        $noTee->setTemplate($template);
        $node = $template->render('test.html.php', ['titleExtension' => ' is cool']);
        $this->assertEquals(
            (string)_document(
                _html(
                    _head(
                        _title('NoTee is cool')
                    ),
                    _body(
                        'Hello Hello World World World'
                    ),
                )
            ),
            (string)$node
        );
    }
}