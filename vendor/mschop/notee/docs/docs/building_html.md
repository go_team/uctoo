Building html with NoTee is straight forward. This guide shows you, how to build html with NoTee and what sugar it gives
you.

## Global Functions vs. Method calls

There are two ways, to use NoTee. You can either directly use the methods of the NodeFactory class, or you can use the 
`global.php` to use a less verbose syntax. See [Use global functions](http://127.0.0.1:8000/##use-global-functions-recommended)
for details.

The syntax is just a little different:

    <?php
    
    $nf->div(); // method call
    // vs.
    _div(); // global function
    
## Tree

The tree is built, by defining the children of a node when creating a new node:

    <?php
    _div(
        _span('Hallo'), // first child of <div>
        _span('World'), // second child of <div>
    );
    
You can also provide the children as an array. This is needed for enabling functional constructs
(see [Imperative vs. Functional](/#building_html/#imperative-vs-functional)) and I would not
recommend writing it that way (because it's ugly):

    <?php
    _div(
        [
            _span('Hello'),
            _span('World'),
        ]
    );
    
Please note, that this does not conflict with the attribute array (see [Attributes](/#building_html/#attributes)).
If you provide an attribute array, the attribute array must be the first array in the parameter list.

## Generate Result

NoTee generates a node tree. All node types can be casted to a string. Therefore you simply need to convert the tree to
a string for getting the result html:

    <?php
    $node = _span();
    $html = (string)$node; // $html now contains "<span></span>"
    
You can also directly echo the node. Because PHP does type juggling, it casts the node tree implicitly to a string:

    <?php
    echo _span();
    
## Immutability

Nodes are immutable. Therefore they can be reused countable times and without unexpected behaviour:

    <?php
    $node = _span('Hallo ');
    _div($node, $node); // this results in: <div><span>Hallo </span><span>Hallo </span></div>
    
## Attributes

When creating an html node, you can declare the attributes for the the node by providing an associative array as the
first argument:

    <?php
    
    echo _div(
        ['class' => 'container', 'data-something' => 10],
        'Hello World'
    );

This would produce the following output:

    <div class="container" data-something="10">Hello World</div>
    
You can omit the attribute array. The following lines are identical:

    <?php
    _div([], 'Hello World');
    _div('Hello World');
    
## Text

To output text, you simply can provide the string as if you would create a child node. NoTee transforms a string into a
TextNode in the node tree. The following lines are identical:

    <?php
    _span('Hello World');
    _span(_text('Hello World'));
    
Please note, that both strings are escaped for security reasons. If you need to output HTML, you need to create a
RawNode.

## Raw

If you need to output plain HTML (without escaping) you need to use the raw method:

```
<?php

_span('<br />'); // result in: <span>&lt;br /&gt;</span>
_span(_raw('<br />')); // reuslts in <span><br /></span>
```

## Imperative vs. Functional

In NoTee you can use plain PHP. You can use all control flow constructs that are available in PHP. Nevertheless we would
recommend using a functional programming style. If you use a functional programming style, your code will be clearer
and less verbose when using NoTee.

For making your live easier, you should use some library, that provides functional methods like `map` (most important).
Which one you use, is really up to you and depends on your personal taste. The following code uses functions from the
php library [lstrojny/functional-php](https://packagist.org/packages/lstrojny/functional-php).

This code uses imperative style for outputting a list of elements:

    <?php
    
    $items = [1, 2, 3, 4];
    
    $liElements = [];
    foreach ($items as $item) {
        $liElements[] = _li($item);
    }
    
    echo _ul(
        $liElements
    )
    
Yes, this is ugly, and I don't like that. This example produces the same result but uses function style:

    <?php
    
    use function map;
    
    $items = [1, 2, 3, 4];
    
    echo _ul(
        map($items, fn($item) => _li($item))
    );
    
Again, it's up to you to choose your style. I would recommend the functional style, but the imperative style also works.