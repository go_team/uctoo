The only officially supported installation method is through composer:

    composer require mschop/notee
    
If you don't have composer available, follow the composer installation instructions under:
[Composer Installation](https://getcomposer.org/doc/00-intro.md)

## Basic Usage

Using NoTee is straight forward. You just need to create an instance of the class NodeFactory:

    <?php
    
    require 'vendor/autoload.php';
    
    namespace YourNamespace;
    
    use NoTee\NodeFactory;
    use NoTee\DefaultEscaper;
    use NoTee\UriValidator;
    use NoTee\BlockManager;
    
    $nf = new NodeFactory(
        new DefaultEscaper('utf-8'),
        new UriValidator(),
        new BlockManager()
    );
    
    // now you can already build html:
    
    echo $nf->document(
        $nf->html(
            $nf->head(
                ...
            ),
            $nf->body(
                ...
            ),
        )
    );
    
## Use global functions (recommended)

You can make use of the global functions. Whether one should use the global functions is a religious question, because
this feature relies on global state. Using the global functions enables less verbose code:

    <?php
    
        
    require 'vendor/autoload.php';
    
    namespace YourNamespace;
    
    use NoTee\NodeFactory;
    use NoTee\DefaultEscaper;
    use NoTee\UriValidator;
    use NoTee\BlockManager;
    
    global $noTee;
    $noTee = new NodeFactory(
         new DefaultEscaper('utf-8'),
         new UriValidator(),
         new BlockManager()
     );
     require 'vendor/mschop/notee/global.php';
     
     // Now you can use global functions. All functions are prefixed with _ for reducing the probability for naming 
     // collisions
     
     echo _document(
        _html(
            _head(
                ...
            ),
            _body(
                ...
            )
        )     
     );
    
## Use Template-Feature

If you need extensibility (block system and template hierarchy) you should use the template feature.

    <?php
    
        
    require 'vendor/autoload.php';
    
    namespace YourNamespace;
    
    use NoTee\NodeFactory;
    use NoTee\DefaultEscaper;
    use NoTee\UriValidator;
    use NoTee\BlockManager;
    use NoTee\Template;
        
    global $noTee;
    $noTee = new NodeFactory(
         new DefaultEscaper('utf-8'),
         new UriValidator(),
         new BlockManager()
     );
     require 'vendor/mschop/notee/global.php';
     
     $directories = [
        'templates/base',
        'templates/child',
     ];
     $template = new Template($directories, $noTee);
     $noTee->setTemplate($template);
     
A details explanation of the template system can be found in the
[Template Documentation](/#extensibility/#template-functionality).