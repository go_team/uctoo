<?php


namespace NoTee;


class Scoping
{
    public static function includeScoped(string $file, array $context)
    {
        foreach ($context as $key => $value) {
            $$key = $value;
        }
        unset($context);
        return include($file);
    }

    public static function requireScoped(string $file, array $context)
    {
        foreach ($context as $key => $value) {
            $$key = $value;
        }
        unset($context);
        return require($file);
    }
}