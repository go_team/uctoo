<?php

declare(strict_types=1);

namespace NoTee\Nodes;


use NoTee\NodeInterface;

class DocumentNode implements NodeInterface
{
    protected NodeInterface $topNode;

    public function __construct(NodeInterface $topNode)
    {
        $this->topNode = $topNode;
    }

    public function __toString(): string
    {
        return "<!DOCTYPE html>{$this->topNode}";
    }
}
