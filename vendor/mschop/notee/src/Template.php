<?php


namespace NoTee;


class Template implements TemplateInterface
{
    protected AbstractNodeFactory $nodeFactory;
    protected array $templateDirs;

    /**
     * Template constructor.
     * @param array $templateDirs
     * @param AbstractNodeFactory $nodeFactory
     */
    public function __construct(array $templateDirs, AbstractNodeFactory $nodeFactory)
    {
        $this->templateDirs = $templateDirs;
        $this->nodeFactory = $nodeFactory;
    }

    public function render(string $template, array $context = []): NodeInterface
    {
        $template = trim($template, '/\\');
        $dirs = array_reverse($this->templateDirs);
        foreach ($dirs as $dir) {
            $path = $dir . '/' . $template;
            if (file_exists($path)) {
                $return = Scoping::requireScoped($path, $context);
                if ($return === null) {
                    return $this->nodeFactory->wrapper();
                } elseif ($return !== 1) {
                    return $return;
                }
            }
        }
        throw new \InvalidArgumentException("Template File '$template' either does not exist or does not return an instance of " . NodeInterface::class);
    }
}