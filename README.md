# UCToo  
UCToo是一套云原生分布式SaaS应用开发基础设施。符合SaaS成熟度模型Level5级标准，实现SaaS统一应用实例模板开发规范，采用开源、通用、厂商中立的技术选型。内置众多开发者效率工具，可快速进行SaaS应用的开发和运营，适合作为SaaS平台、产业互联网等产品的起始项目。开发成果可共享，可互联互通，可在UCToo应用市场商业化分发。  

## 概述
UCToo是一系列实现云原生分布式SaaS应用开发的最佳实践的总称。以下UCToo技术体系架构请参考，并随业界技术发展持续迭代。  

![UCToo技术体系架构](https://gitee.com/UCT_admin/materials/raw/master/uctoo/Tech%20Architect.png)  

本代码库主要包含了UCToo APP server的一个PHP技术选型的具体实现版本。

## UCToo技术体系索引

1. [https://gitee.com/UCT/uctoo-app-server](https://gitee.com/UCT/uctoo-app-server) UCToo APP server SaaS应用实例模板服务器端开发规范  
2. [https://gitee.com/uctoo/uctoo](https://gitee.com/uctoo/uctoo) UCToo APP server PHP技术选型的具体实现版本,采用[CatchAdmin](https://github.com/JaguarJack/catch-admin)，支持管理后台模块化低代码开发    
3. [https://gitee.com/UCT/uctoo-api-client](https://gitee.com/UCT/uctoo-api-client) UCToo api client 用于对接UCToo APP server服务端接口的客户端SDK规范。技术选型为[Vuex ORM Axios](https://github.com/vuex-orm/plugin-axios) ，具体可参考[UCToo Vue Editor](https://gitee.com/UCT/uctoo-vue-editor) 项目的应用示例。  
4. [https://gitee.com/UCT/uctoo-api-client-php](https://gitee.com/UCT/uctoo-api-client-php) UCToo api client PHP技术选型的具体实现版本，[UCToo](https://gitee.com/uctoo/uctoo)已内置开源版  
5. [https://gitee.com/UCT/uctoo-app-server-vue](https://gitee.com/UCT/uctoo-app-server-vue) UCToo APP server PC端管理后台前端界面,采用[CatchAdmin Vue](https://github.com/JaguarJack/catch-admin-vue)  
6. [UCToo APImanager](https://gitee.com/uctoo/uctoo/tree/master/catch/apimanager) 模块是一个用于API管理、测试、协作的SaaS应用，[UCToo](https://gitee.com/uctoo/uctoo)已内置开源版  
7. [UCToo wechatopen manager](https://gitee.com/UCT/wechatopen)  模块是一个用于管理微信第三方平台的后台管理应用，[UCToo](https://gitee.com/uctoo/uctoo)已内置开源版  
8. [UCToo wechatopen server](https://gitee.com/uctoo/uctoo/tree/master/app/uctoo/wechatopen)  模块是一个用于对接微信生态所有API的标准微信第三方平台前置服务器应用，[UCToo](https://gitee.com/uctoo/uctoo)已内置开源版  
9. [UCToo Codelabs](https://gitee.com/uctoo/uctoo/tree/master/catch/develop) Codelabs模块是一个极简分布式端云协作开发工具，实践代码定义规范的理念，提供开发者可视化填空式低代码/无代码编程体验。与大多数采用json解析器的低代码/无代码技术路线产品不同，codelabs采用了语法分析合并生成源码的方案，使得codelabs具有极低的学习成本，和极简的可扩展性，同时具有广泛的普适性。
10. [UCToo Vue Editor](https://gitee.com/UCT/uctoo-vue-editor) 模块是一个可视化动态页面搭建独立项目。UCToo线上运营版本地址 https://vueeditor.uctoo.com ，帐号与 www.uctoo.com 帐号相同。开源项目地址[https://gitee.com/UCT/uctoo-vue-editor](https://gitee.com/UCT/uctoo-vue-editor)。实践了全栈模型同构的理念，为低代码、无代码、可视化代码生成等开发需求提供了更加规范化的架构风格和高效率的开发基础设施。  
11. [UCT UI](https://github.com/crazyfdf/uct-ui)  是一个多端兼容的前端UI框架,采用[uni-app uView](https://gitee.com/xuqu/uView)实现，支持移动端应用低代码快速开发    
12. [UCToo docker](https://gitee.com/UCT/uctoo-docker) 云原生分布式部署UCToo APP server 应用实例的容器项目，支持UCToo SaaS应用实例一键部署至公有云平台  
13. [SaaS共享数据模型](https://www.kancloud.cn/doc_uctoo/uctoo_dev/2108288) 符合多商户多应用、多端统一用户身份的一套SaaS共享数据模型，[UCToo](https://gitee.com/uctoo/uctoo)已内置开源版  
14. [UCToo应用市场](https://appstore.uctoo.com) 支持云原生分布式SaaS应用进行商业化分发的应用市场,连接开发者和商户的开放式交易平台  
15. [微用户小程序](https://gitee.com/UCT/miniprogram-demo) 开发者服务小程序，提供开源项目商业化全流程运营支撑，开源版本修改自[微信官方Demo小程序](https://github.com/wechat-miniprogram/miniprogram-demo)，提供开发者UCToo开发示例
16. [www.uctoo.com](https://www.uctoo.com) UCToo产品官网，采用UCToo APP server 开发的，支持云原生分布式的新一代SaaS服务运营支撑平台。
17. [www.uctoo.org](https://www.uctoo.org) UCTOO通用云技术开源组织，为符合level5级SaaS产品开源项目提供孵化、技术支持和商业化服务，收集和整理业界最佳实践。（筹备中）
  
## 主要特性  

  本项目的设计目标是提供开发人员，可以主要通过定义应用模块的数据结构，即可生成应用模块的管理后台、API接口以及默认移动应用，使得无论掌握何种技术栈的后台开发人员还是前端开发人员，都可以采用本项目快速的开发多端兼容的数字化系统和应用，并可进行灵活的二次开发。
  
1.  先进技术，主流应用，开源开放。
2.  模块可拆卸，系统只保留核心的功能，其余功能通过扩展模块来实现。
3.  支持云原生应用开发，支持多商户多应用管理SaaS平台特性，可在多数云平台环境独立安装部署，支持云原生部署。
4.  支持微信小程序云开发特性。
5.  实现了微信第三方平台对接的中控服务器逻辑，并可灵活自定义与微信公众平台、微信开放平台的各种业务交互，由于第三方平台开发方式是微信交易组件、批量云开发等的依赖基础，因此建议微信相关应用开发都采用第三方平台方式。
6.  实现了微信第三方平台应用模块化开发，各种增值应用都可以独立安装及卸载，支持模块商业化分发和部署。
7.  已集成微信第三方平台批量代云开发，支持去中心化部署商户Web端管理平台和小程序端应用模板。
8.  catchadmin可实现商户Web端管理平台低代码快速开发，UCToo低代码模块可实现小程序、APP多端低代码快速开发。


## 产品架构
1.  系统融合了众多优秀开源项目，采纳通用技术，每周持续集成业界最佳实践。UCToo开发团队扩展了微信应用开发相关功能，对接了微信公众平台和开放平台的所有接口，使其允许在任何类型的微信应用项目中使用。
2.  采用catchadmin、easywechat开发UCToo 2.0版本，相关资料请参考https://www.catchadmin.com/ 、https://www.easywechat.com
3.  使用了[uctoo/think-easywechat SDK](https://gitee.com/UCT/think-easywechat) 集成catchadmin (TP6+VUE) 和 easywechat 4，支持微信第三方平台、微信小程序云开发、微信支付服务商等特性
4.  第三方平台配置信息保存在 wechatopen 表。授权到第三方平台的公众号/小程序等帐号信息保存在 wechatopen_applet表，wechatopen 表的参数用来初始化 easywechat SDK，wechatopen_applet表的参数用来作为第三方平台代公众号/小程序实现业务的帐号信息

```diff
- UCToo V2 技术选型采用了前后端分离的catchadmin框架，
  同时结合腾讯云CloudBase云原生开发平台的特性，
  可实现SaaS应用实例模板去中心化部署，
  使得本项目符合Level 5 级SaaS产品的主要特性
  （Level 5级SaaS产品定义请参考本项目开发文档 https://www.kancloud.cn/doc_uctoo/uctoo_dev ）
```

## 安装教程

可通过以下开源项目进行二次开发。

1. [UCToo-docker项目源码](https://gitee.com/UCT/uctoo-docker)  https://gitee.com/UCT/uctoo-docker
2. [UCToo应用模板服务器端PHP项目](https://gitee.com/uctoo/uctoo)  https://gitee.com/uctoo/uctoo    [![](https://main.qcloudimg.com/raw/67f5a389f1ac6f3b4d04c7256438e44f.svg)](https://console.cloud.tencent.com/tcb/env/index?action=CreateAndDeployCloudBaseProject&appUrl=https%3A%2F%2Fgitee.com%2FUCT%2Fuctoo-docker&branch=master)  
3. [UCToo应用模板PC端VUE项目](https://gitee.com/UCT/uctoo-app-server-vue)  https://gitee.com/UCT/uctoo-app-server-vue    [![](https://main.qcloudimg.com/raw/67f5a389f1ac6f3b4d04c7256438e44f.svg)](https://console.cloud.tencent.com/tcb/env/index?action=CreateAndDeployCloudBaseProject&appUrl=https%3A%2F%2Fgitee.com%2FUCT%2Fuctoo-app-server-vue&branch=master)


### cloudbase 一键云端部署说明
1. 推荐使用cloudbase 一键部署。如安装时提示无本地环境，请先登录腾讯云->云开发cloudbase->环境总览开通一个云开发环境（目前只支持按量计费）。
2. 必须先一键部署UCToo应用模板服务器端PHP项目。项目部署完成后，在腾讯云->云开发cloudbase->我的应用，可以获取到后台访问地址，此地址即为后端baseAPI地址。
3. 通过腾讯云->云开发cloudbase->云托管->uctooserver服务->uctooserver-xxx版本->实例->Webshell登录后端实例（如果版本下没有实例，请先访问后端首页，激活一个实例运行），命令行 cd 至 /var/www/html/uctoo目录下，运行 php think uctoo:install 命令初始化数据库（如果安装命令运行失败，提示数据库休眠，请稍等几秒待数据库激活后再运行一遍命令。uctoo:install命令目前只支持cloudbase一键安装环境）。
4. 再一键部署UCToo应用模板PC端VUE项目。项目初始化部署时，用户需要填写以上步骤2获取到的baseAPI地址。前端VUE项目即可从baseAPI获取后端服务。部署完成后，在腾讯云->云开发cloudbase->我的应用，可以获取到前端访问地址。
5. 可通过腾讯云->云开发cloudbase->静态网站托管查看已部署的前端项目代码。
6. 可以通过腾讯云->云原生数据库TDSQL-C->登录，管理数据库。如果数据库未启动，请先启动。
7. 可以通过腾讯云->文件存储管理挂载的CFS文件系统。文件系统挂载于后端 /var/www/html 目录，用于保存用户UCToo APP server代码和静态资源文件等内容。
8. 初始化安装后，cloudbase自动分配的前后端访问地址与用户自定义配置的前后端域名不一致，需在云托管->服务配置->HTTP访问服务配置（后端baseAPI地址），以及静态网站托管->基础配置->自定义域名配置（前端访问网址），用户设置的域名才可以生效。
9. 安装完成后，可用初始化安装设置的超管帐号登录管理后台，通过系统管理->模块管理功能安装更多内置模块，也可以到UCToo应用市场 https://appstore.uctoo.com 购买更多模块安装至本地实例。

### CI/CD 建议
1. 一键云端部署将git库的代码复制到了CFS持久化运行和保存变更。
2. 可以fork 源码库进行二次开发。修改Dockerfile中的git 地址部署二次开发版本。可通过删除CFS中的/var/www/html/uctoo/public/index.php文件进行覆盖安装。
3. 可在Webshell中 /var/www/html/uctoo/ 目录运行 php think uctoo:install -r 命令重置数据库内容至初始安装。
4. 产品内置API管理模块，可提供开发测试、持续迭代等基础特性。

### 运行环境依赖

  PHP >= 7.1.0     
  Mysql >= 5.5.0 (需支持innodb引擎)  
  PDO PHP Extension     
  MBstring PHP Extension     
  CURL PHP Extension     
  ZIP Extension    
  Composer  
    
### 分步骤安装  
1.  安装catchadmin，请参考 https://www.catchadmin.com/ 相关文档
2.  本项目后端PHP源码地址 https://gitee.com/uctoo/uctoo , 前端VUE源码地址 https://gitee.com/UCT/uctoo-app-server-vue
3.  此项目依赖于以下扩展https://gitee.com/UCT/think-easywechat ,在项目根目录运行命令 composer require uctoo/think-easywechat:dev-master 安装
4.  在微信开放平台open.weixin.qq.com 注册认证开发者帐号，创建第三方平台，配置第三方平台参数，其中授权事件接收URL 填写为 https://域名/wechatopen/authevent ，消息与事件接收URL 填写为 

```php
https://域名/wechatopen/eventmessage/appid/$APPID$
```
4.  在wechatopen插件第三方平台菜单，添加微信第三方平台配置信息，仅需填写appid、appsecret、encodingAesKey、token 4个参数
5.  在微信开放平台open.weixin.qq.com 提交测试第三方平台，通过测试后提交全网发布
6.  授权公众号、小程序到此第三方平台，进行增值应用开发。

### 云原生安装
1.  可在 https://www.uctoo.com 注册开发者帐号，登录管理后台，通过云开发功能模块，即可采用云原生方式开通和部署一套独立的UCToo运行实例。

### docker安装
  可参考uctoo-docker项目 https://gitee.com/UCT/uctoo-docker

## 使用手册
  具体请参考 https://www.kancloud.cn/doc_uctoo/manual
  
## 开发说明
  具体请参考开源版开发手册 https://www.kancloud.cn/doc_uctoo/uctoo_dev 及 本开源项目示例

## 问题反馈
  开发者交流QQ群984748053
  更多信息请关注UCToo微信第三方运营平台 https://www.uctoo.com   

## 参考资料：
  UCToo     使用文档 https://www.kancloud.cn/doc_uctoo/manual  
            开发文档 https://www.kancloud.cn/doc_uctoo/uctoo_dev  
            UCT UI文档 https://uct-1257264070.cos-website.ap-guangzhou.myqcloud.com  
            演示 https://www.uctoo.com
            
  catchadmin 文档 https://www.catchadmin.com/docs/   
            演示 https://www.catchadmin.com/  
          
  ThinkPHP  文档 https://www.thinkphp.cn   
  
  easywechat 文档 https://www.easywechat.com/   
  
  微信开放平台  https://open.weixin.qq.com/   
  微信公众平台  https://mp.weixin.qq.com/   
  微信支付  https://pay.weixin.qq.com   
  腾讯云开发  https://cloud.tencent.com/product/tcb   
              
  UCTOO开源组织  https://www.uctoo.org/  筹备中 

  CNCF 官网 https://www.cncf.io/   
       项目 https://github.com/cncf   
     
## 版权信息  
UCToo遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2014-2021 by UCToo (https://www.uctoo.com)

All rights reserved