<?php
/**
 * @filename  Upload.php
 * @createdAt 2020/1/25
 * @project  https://github.com/yanwenwu/catch-admin
 * @document http://doc.catchadmin.com
 * @author   JaguarJack <njphper@gmail.com>
 * @copyright By CatchAdmin
 * @license  https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt
 */
namespace app\uctoo\common\api;

use app\BaseController;
use catchAdmin\system\model\Attachments;
use catcher\base\CatchController;
use catcher\base\CatchRequest;
use catcher\CatchResponse;
use catcher\CatchUpload;
use catcher\exceptions\FailedException;
use think\Request;

class Upload extends BaseController
{
    protected $attachment;

    public function initialize()
    {
        $this->attachment = new Attachments;
    }

  /**
   * image upload
   *
   * @time 2020年01月25日
   * @param CatchRequest $request
   * @param CatchUpload $upload
   * @return \think\response\Json
   */
    public function image(Request $request, CatchUpload $upload): \think\response\Json
    {
        $images = array_values($request->file());

        if (!count($images)) {
            throw new FailedException('请选择图片上传');
        }

        return CatchResponse::success($upload->checkImages($images)->multiUpload(
            count($images) < 2 ? $images[0] : $images
        ));
    }

  /**
   * file upload
   *
   * @time 2020年01月25日
   * @param CatchRequest $request
   * @param CatchUpload $upload
   * @return \think\response\Json
   */
    public function file(Request $request, CatchUpload $upload): \think\response\Json
    {
        $files = $request->file();

        return CatchResponse::success($upload->checkFiles($files)->multiUpload($files['file']));
    }
}
