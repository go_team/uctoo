<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: UCToo <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace app\uctoo\wechatopen\api;

use app\BaseController;
use uctoo\middleware\AppletTest;
use uctoo\middleware\Auth;
use EasyWeChat\Factory;
use catchAdmin\wechatopen\model\Applet;
use uctoo\ThinkEasyWeChat\OpenPlatform\Ability\Client;
use EasyWeChat\OpenPlatform\Application;
use uctoo\ThinkEasyWeChat\MiniShop\Base\ServiceProvider;
use think\facade\Log;

class Wechatopen extends BaseController
{
  //  protected $middleware = [
  //      Auth::class,
  //      AppletTest::class
  //  ];

    /**
     * applet 实例
     * @var \catchAdmin\wechatopen\model\Applet
     */
    protected $applet;

    // 初始化
    protected function initialize()
    {
        $appid = input('appid');
        $this->applet = Applet::where('appid',$appid)->find();
    }

    /**
     * product category  获取类目详情
     *
     * @param int $f_cat_id
     * @return array|Collection|object|ResponseInterface|string
     * @throws InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function productCategoryGet(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $f_cat_id = input('f_cat_id');
        if(!is_numeric($f_cat_id)){
            $f_cat_id = 0;
        }
        $res = $minishop_base->productCategoryGet($f_cat_id);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productBrandGet(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $res = $minishop_base->productBrandGet();
        return $this->wxResult($res, 'minishop_base');
    }

    public function productDeliveryGet_freight_template(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $res = $minishop_base->getFreightTemplate();
        return $this->wxResult($res, 'minishop_base');
    }

    public function productStoreGet_shopcat(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $res = $minishop_base->getShopcat();
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuAdd(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $spu_data = input('post.');//'';
        $res = $minishop_base->productSpuAdd($spu_data);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuGet(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuGet($body);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuGet_list(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuGetlist($body);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuSearch(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuSearch($body);
        return $this->success('ok',$res);
       // $this->wxResult($res, 'minishop_base');  //官方文档与实际返回数据结构不一致
    }

    public function productSpuUpdate(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuUpdate($body);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuListing(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuListing($body);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuDelisting(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuDelisting($body);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productStoreGet_info(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base; //

        $res = $minishop_base->productStoreGetinfo();
        //$this->request->mapp->registerProviders([\uctoo\ThinkEasyWeChat\OpenPlatform\Authorizer\MiniProgram\Basic\ServiceProvider::class]);
        /**
         * @var \app\util\src\OpenPlatform\Authorizer\MiniProgram\Basic\Client $basic
         */
        return $this->wxResult($res, 'minishop_base');
    }

    public function productImgUpload(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $upload_type = input('upload_type');
        $resp_type = input('resp_type');
        $img_url = input('img_url');
        $height = input('height');
        $width = input('width');
        $res = $minishop_base->productImgUpload($upload_type,$resp_type,$img_url,$height,$width);
        return $this->wxResult($res, 'minishop_base');
    }

    public function snsComponentJscode2session(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $js_code = input('js_code');
        $res = $miniapp->auth->session($js_code);
        $res['errcode'] = 0;    //微信接口返回值里的errcode没有了，还得自己添加！！！
        $res['errmsg'] = '请求成功';
        return $this->wxResult($res);
    }

    public function wxaGetwxacodeunlimit(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $scene = input('scene');
        $optional = null;
        if(input('page')){
            $optional['page'] = input('page');
        }
        if(input('width')){
            $optional['width'] = input('width');
        }
        if(input('auto_color ')){
            $optional['auto_color '] = input('auto_color ');
        }
        if(input('line_color')){
            $optional['line_color'] = input('line_color');
        }
        $savepath = public_path('images').'temp';
        //安全考虑，不支持自定义文件保存路径
        //if(input('savepath')){
        //    $savepath = input('savepath');
        //}
        $savename = 'wxacodeunlimit.png';
        if(input('savename')){
            $savename = input('savename');
        }
        $res = $miniapp->app_code->getUnlimit($scene, $optional);
        if ($res instanceof \EasyWeChat\Kernel\Http\StreamResponse) {
            $filename = $res->save($savepath,$savename);
        }
        $result['errcode'] = 0;    //微信接口返回值里的errcode没有了，还得自己添加！！！
        $result['errmsg'] = '请求成功';
        $result['file'] = strstr( $savepath, '/images').DIRECTORY_SEPARATOR.$filename;
        return $this->wxResult($result);
    }

    public function wxaPluginList(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $action = input('action');
        $plugin_appid = input('plugin_appid');
        $reason = input('reason');

        $res = $miniapp->plugin->list();
        return $this->wxResult($res);
    }

    public function wxaPluginApply(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $action = input('action');
        $plugin_appid = input('plugin_appid');
        $reason = input('reason');

        $res = $miniapp->plugin->apply($plugin_appid);
        return $this->wxResult($res);
    }

    public function wxaGetUserPhoneNumber(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $code = input('code');

        $res = $miniapp->phone_number->getUserPhoneNumber($code);
        return $this->wxResult($res);
    }

    public function messageWxopenActivityidCreate(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);

        $res = $miniapp->activity_message->createActivityId();
        return $this->wxResult($res);
    }


}