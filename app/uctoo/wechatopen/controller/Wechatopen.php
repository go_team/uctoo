<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace app\uctoo\wechatopen\controller;

use app\BaseController;
use think\facade\Request;
use uctoo\util\EventHandler\miniProgram\AuditMessageHandler;
use uctoo\util\EventHandler\openPlatform\AuthorizedEventHandler;
use uctoo\util\EventHandler\openPlatform\FullPublicMessageHandler;
use uctoo\util\EventHandler\openPlatform\EventMessageHandler;
use uctoo\util\EventHandler\openPlatform\RegisterEventHandler;
use uctoo\util\EventHandler\openPlatform\UnauthorizedEventHandler;
use uctoo\util\EventHandler\openPlatform\ComponentVerifyTicketEventHandler;
use uctoo\util\EventHandler\openPlatform\UpdateAuthorizedEventHandler;
use EasyWeChat\Kernel\Exceptions\BadRequestException;
use EasyWeChat\Kernel\Exceptions\InvalidArgumentException;
use EasyWeChat\Kernel\Exceptions\InvalidConfigException;
use EasyWeChat\Kernel\Messages\Message;
use EasyWeChat\OpenPlatform\Application;
use EasyWeChat\OpenPlatform\Server\Guard;
use think\facade\Cache;
use think\facade\Log;
use think\facade\Session;
use uctoo\ThinkEasyWeChat\Facade;
use catchAdmin\wechatopen\model\Applet;

class Wechatopen extends BaseController
{
    protected $openPlatform;

    /**
     * 微信第三方平台授权事件接收URL
     * @param Application $app
     */
    public function authEvent(Application $app){
        try{
            // 授权事件
            $app->server->push(new ComponentVerifyTicketEventHandler($app),Guard::EVENT_COMPONENT_VERIFY_TICKET);
            // 授权事件
            $app->server->push(new AuthorizedEventHandler($app),Guard::EVENT_AUTHORIZED);
            // 更新授权事件
            $app->server->push(new UpdateAuthorizedEventHandler($app),Guard::EVENT_UPDATE_AUTHORIZED);
            // 取消授权事件
            $app->server->push(new UnauthorizedEventHandler($app),Guard::EVENT_UNAUTHORIZED);
            // 小程序快速注册事件
            $app->server->push(new RegisterEventHandler($app),Guard::EVENT_THIRD_FAST_REGISTERED);

            $app->server->serve()->send();
        }catch (\Exception $e){
            abort(404);
        }

    }

    /**
     * 第三方平台微信消息接口入口
     * 所有发送到微信的消息都会推送到该操作
     * 所以，微信第三方平台后台填写的api地址则为该操作的访问地址
     * 在open.weixin.qq.com 第三方平台配置的 消息与事件接收URL(服务器地址)  http://域名/wechatopen/eventmessage/appid/$APPID$
     *
     * @param Application $app
     * @param null $appid
     * @throws BadRequestException
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     * @throws \ReflectionException
     */
    public function EventMessage(Application $app,$appid = null){

        if(is_null($appid)){
            abort(404);
        }
        $preAccount = [  //第三方平台全网发布测试帐号
            'wx570bc396a51b8ff8' => 'officialAccount',
            'wxd101a85aa106f53e' => 'miniProgram',
        ];

        if(isset($preAccount[$appid])){  //全网发布测试
            $server = $app->{$preAccount[$appid]}($appid)->server;
            $server->push(new FullPublicMessageHandler($app,$appid,$preAccount[$appid]));
        }else{
            $server = $app->officialAccount($appid)->server;
            $server->push(new EventMessageHandler($appid));
        }
        $server->serve()->send();
    }

}