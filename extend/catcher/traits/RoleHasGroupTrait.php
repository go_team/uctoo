<?php
declare(strict_types=1);

/**
 * 在需要支持分组方法的控制器中使用此trait，控制器中必须有role成员变量，且role所赋值的模型需支持RoleHasGroupTrait模型
 * 在route.php中添加addGroups、getGroups、delGroups对应的路由，并且注意接口权限
 * @filename  RoleHasGroupTrait.php
 * @createdAt 2022/2/22
 * @project  https://gitee.com/uctoo/uctoo
 * @document https://gitee.com/uctoo/uctoo
 * @author   UCToo <contact@uctoo.com>
 * @copyright By UCToo
 * @license  https://gitee.com/uctoo/uctoo/raw/master/license.txt
 */
namespace catcher\traits;

use catcher\CatchResponse;

trait RoleHasGroupTrait
{

    /**
     * 添加角色与用户组关联
     *
     * @time 2022年02月22日
     * @param \think\Request $request
     * @param int role_id 角色表id
     * @param string groups 用户组id，以逗号分隔
     * @return \think\response\Json  返回新增的中间表数据
     */
    public function addGroups(\think\Request $request)
    {
        $role_id = input('role_id');
        $role = $this->role->findBy($role_id);
        $hasGroup = $role->getGroups()->toArray();
        $hasGroupIds = array_column($hasGroup,'id');
        $addGroup = explode(',',$request->param('groups'));
        $diff = array_diff($addGroup,$hasGroupIds);  //只添加差集，以使中间表无多余数据
        $res = $role->attachGroups($diff);
        return CatchResponse::success($res);
    }

    /**
     * 查询角色与用户组关联
     *
     * @time 2022年02月22日
     * @param \think\Request $request
     * @param int role_id 角色表id
     * @return \think\response\Json
     */
    public function getGroups(\think\Request $request)
    {
        $role_id = input('role_id');
        $role = $this->role->findBy($role_id);
        $res = $role->getGroups();
        return CatchResponse::success($res);
    }

    /**
     * 删除角色与用户组关联
     *
     * @time 2022年02月22日
     * @param \think\Request $request
     * @param int role_id 角色表id
     * @param string groups 用户组id，以逗号分隔
     * @return \think\response\Json
     */
    public function delGroups(\think\Request $request)
    {
        $role_id = input('role_id');
        $role = $this->role->findBy($role_id);
        $res = $role->detachGroups(explode(',',$request->param('groups')));
        return CatchResponse::success($res);
    }
}