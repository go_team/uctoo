<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\library;


use EasyWeChat\OfficialAccount\Application;
use think\facade\Config;
use EasyWeChat\Payment\Application as Payment;

class OfficialAccount
{
    /**
     * @param array $options
     * @return Application
     */
    public static function instance($options = []){
        $config = Config::get('official_account');
        $app = new Application(array_merge($config,$options));
        $app->rebind('cache',app('cache'));
        return $app;
    }

    public static function payment($options = []){
        $config = Config::get('payment');
        return new Payment(array_merge($config,$options));
    }

    /**
     * 微信扫码登录
     * @param array $options
     * @return Application
     */
    public static function wxLoginInstance($options = []){
        $config = Config::get('wxlogin');
        $app = new Application(array_merge($config,$options));
        $app->rebind('cache',app('cache'));
        return $app;
    }

}