<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\library;


use EasyWeChat\Factory;

class Dev
{
    const DEV_APP_ID = '';  //TODO:从管理后台配置获取
    const DEV_APP_SECRET = '';
    const DEV_APP_ENV = '';

    public static function mpInstance(){
        $config = [
            'app_id' => self::DEV_APP_ID,
            'secret' => self::DEV_APP_SECRET,
            'env' => self::DEV_APP_ENV
        ];
        $app = Factory::miniProgram($config);
        $app->rebind('cache',app('cache'));
        return $app;
    }
}