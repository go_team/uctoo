<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\EventHandler\openPlatform;


use app\model\saas\FastRegister;
use catchAdmin\wechatopen\model\Applet;
use think\facade\Cache;
use uctoo\ThinkEasyWeChat\Facade;
use EasyWeChat\Kernel\Contracts\EventHandlerInterface;
use EasyWeChat\OpenPlatform\Application;
use think\facade\Db;

class RegisterEventHandler implements EventHandlerInterface
{
    /**
     * @var Application
     */
    protected $app;
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function handle($payload = null)
    {
        $field = ['name','code','legal_persona_wechat','legal_persona_name'];
        $params = array_intersect_key($payload['info'],array_flip($field));
        $task_sn = make_sign($params);
        $task = (new FastRegister())->where(['task_sn' => $task_sn])->field(['id','merchant_id'])->find();
        if($task){
            $task->status = $payload['status'];
            if(0 == $payload['status']){
                $task->appid = $payload['appid'];
                Db::name('saas_merchant_account')->where(['id'=>$task['merchant_id']])->update(['applet_appid'=>$payload['appid']]);

                $auth = $this->app->handleAuthorize($payload['auth_code']);
                $authInfo = $this->app->getAuthorizer($payload['appid']);
                $data = [
                    'applet_type' =>  Applet::TYPE_MINI_PROGRAM,
                    'authorize_type' => Applet::AUTHORIZE_TYPE_REGISTER,
                    'appid' => $auth['authorization_info']['authorizer_appid'],
                    'service_type' => $authInfo['authorizer_info']['service_type_info']['id'],
                    'verify_type' => $authInfo['authorizer_info']['verify_type_info']['id'],
                    'nick_name' => $authInfo['authorizer_info']['nick_name'],
                    //'head_img' => $authInfo['authorizer_info']['head_img'],
                    'user_name' => $authInfo['authorizer_info']['user_name'],
                    'principal_name' => $authInfo['authorizer_info']['principal_name'],
                    'business_info' => $authInfo['authorizer_info']['business_info'],
                    'qrcode_url' => $authInfo['authorizer_info']['qrcode_url'],
                    'refresh_token' => $auth['authorization_info']['authorizer_refresh_token'],
                    'status' => Applet::STATUS_AUTHORIZED
                ];
                (new Applet())->save($data);
            }

            $task->save();
        }


    }

}