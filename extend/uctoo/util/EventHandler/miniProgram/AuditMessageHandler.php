<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\EventHandler\miniProgram;


use EasyWeChat\Kernel\Contracts\EventHandlerInterface;

class AuditMessageHandler implements EventHandlerInterface
{
    protected $appid;

    public function __construct($appid)
    {
        $this->appid = $appid;
    }


    public function handle($payload = null)
    {

        //接收事件推送: https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140454
        switch ($payload['Event']) {
            case 'subscribe':  //关注事件, 扫描带参数二维码事件(用户未关注时，进行关注后的事件推送)
                return "欢迎关注2".$this->appid;
                break;
            case 'unsubscribe':  //取消关注事件
                break;
            case 'SCAN':  //扫描带参数二维码事件(用户已关注时的事件推送)
                return "欢迎关注";
                break;
            case 'LOCATION':  //上报地理位置事件
                return "经度: " . $payload['Longitude'] . "\n纬度: " . $payload['Latitude'] . "\n精度: " . $payload['Precision'];
                break;
            case 'CLICK':  //自定义菜单事件(点击菜单拉取消息时的事件推送)
                //return "事件KEY值: " . $payload['EventKey'];
                break;
            case 'VIEW':  //自定义菜单事件(点击菜单拉取消息时的事件推送)
                //return "跳转URL: " . $payload['EventKey'];
                break;
            case 'ShakearoundUserShake':
                //摇一摇事件通知: https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1443448066
               // return 'ChosenBeacon\n' . 'Uuid: ' . $payload['ChosenBeacon']['Uuid'] . 'Major: ' . $payload['ChosenBeacon']['Major'] . 'Minor: ' . $payload['ChosenBeacon']['Minor'] . 'Distance: ' . $payload['ChosenBeacon']['Distance'];
                break;
            case 'wxa_nickname_audit':
                // 名称审核
                // ret nickname  reason
                break;
            case 'weapp_audit_success':
                // 代码审核成功
                // SuccTime
                break;
            case 'weapp_audit_fail':
                // Reason FailTime ScreenShot
                break;
            case 'weapp_audit_delay':
                //Reason DelayTime
                break;
        }

        return false;

    }
}

