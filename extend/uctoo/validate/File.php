<?php
declare (strict_types = 1);

namespace uctoo\validate;

use think\Validate;

class File extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    'file' => [
	        'fileExt' => ['gif', 'jpg', 'jpeg', 'png'],
            'fileMime' => ['image/gif', 'image/jpg', 'image/jpeg', 'image/png'],
            'fileSize' => 2 * 1024 * 1024
        ],
        'cloud_function' => [
            'fileExt' => ['zip'],
            'fileMime' => ['application/zip'],
            'fileSize' => 20 * 1024 * 1024
        ]
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        'file.fileExt' => '文件格式不支持',
        'file.fileMime' => '文件格式不支持',
        'file.fileSize' => '文件超出限制',
    ];
}
