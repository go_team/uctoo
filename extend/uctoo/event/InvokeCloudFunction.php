<?php
declare (strict_types = 1);

namespace uctoo\event;

class InvokeCloudFunction
{
    /**
     * @var 原函数名
     */
    public $func_name;
    /**
     * @var 云函数参数
     */
    public $func_params;
    /**
     * @var 云函数返回数据
     */
    public $resp_data;
    /**
     * @var 拓展参数
     */
    public $extends;

    public function __construct($func_name,$func_params,$resp_data,$extends = [])
    {
        $this->func_name = $func_name;
        $this->func_params = $func_params;
        $this->resp_data = $resp_data;
        $this->extends = $extends;
    }

}
