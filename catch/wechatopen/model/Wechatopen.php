<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------
namespace catchAdmin\wechatopen\model;

use think\facade\Cache;
use catcher\base\CatchModel as Model;

class Wechatopen extends Model
{
    // 表名
    public $name = 'wechatopen';
    // 数据库字段映射
    public $field = array(
        'id',
        // appid
        'appid',
        // appsecret
        'appsecret',
        // encodingAesKey
        'encodingAesKey',
        // component_verify_ticket
        'component_verify_ticket',
        // component_access_token
        'component_access_token',
        // token过期时间
        'token_overtime',
        // 预授权码
        'pre_auth_code',
        // 预授权过期时间
        'pre_code_overtime',
        // 状态
        'status',
        // token
        'token',
        // 创建人ID
        'creator_id',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除
        'deleted_at',
    );

    //自定义初始化
    protected static function init()
    {


    }
    
    /**
     * appid查询
     *
     * @time 2020年06月17日
     * @param $query
     * @param $value
     * @param $data
     * @return mixed
     */
    public function searchAppidAttr($query, $value, $data)
    {
        return $query->whereLike('appid', $value);
    }
}