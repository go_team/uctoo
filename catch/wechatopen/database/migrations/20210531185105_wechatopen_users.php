<?php

use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class WechatopenUsers extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('wechatopen_users', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信开放平台用户' ,'id' => 'id' ,'primary_key' => ['id']]);
        $table->addColumn('user_ids', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '关联users表ID',])
			->addColumn('appid', 'string', ['limit' => 64,'null' => false,'default' => '','signed' => true,'comment' => '关联applet表appid',])
			->addColumn('openid', 'string', ['limit' => 64,'null' => false,'default' => '','signed' => true,'comment' => '普通用户的标识',])
			->addColumn('unionid', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '用户统一标识',])
			->addColumn('nickname', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '普通用户昵称',])
			->addColumn('mobile', 'string', ['limit' => 16,'null' => true,'signed' => true,'comment' => '手机号码',])
			->addColumn('password', 'string', ['limit' => 32,'null' => true,'signed' => true,'comment' => '登录密码',])
			->addColumn('headimgurl', 'string', ['limit' => 256,'null' => true,'signed' => true,'comment' => '用户头像',])
			->addColumn('sex', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => '普通用户性别，1=男性，2=女性',])
			->addColumn('country', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '国家',])
			->addColumn('province', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '省份',])
			->addColumn('city', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '城市',])
			->addColumn('language', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '用户的语言',])
			->addColumn('subscribe', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => '是否订阅该公众号标识，0=未关注，1=关注',])
			->addColumn('subscribe_time', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => '用户关注时间，为时间戳',])
			->addColumn('remark', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '公众号运营者对粉丝的备注',])
			->addColumn('groupid', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => '用户所在的分组ID（兼容旧的用户分组接口）',])
			->addColumn('tagid_list', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '用户被打上的标签ID列表',])
			->addColumn('subscribe_scene', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '返回用户关注的渠道来源',])
			->addColumn('qr_scene', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '二维码扫码场景（开发者自定义）',])
			->addColumn('qr_scene_str', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '二维码扫码场景描述（开发者自定义）',])
			->addColumn('privilege', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '用户特权信息，json数组',])
			->addColumn('loginip', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => 'IP地址',])
			->addColumn('token', 'string', ['limit' => 32,'null' => true,'signed' => true,'comment' => 'token',])
			->addColumn('status', 'string', ['limit' => 32,'null' => true,'signed' => true,'comment' => '状态',])
			->addColumn('access_token', 'string', ['limit' => 256,'null' => true,'signed' => true,'comment' => 'access_token',])
			->addColumn('access_token_overtime', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => 'access_token_overtime',])
			->addColumn('refresh_token', 'string', ['limit' => 256,'null' => true,'signed' => true,'comment' => 'refresh_token',])
			->addColumn('refresh_token_overtime', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => 'refresh_token_overtime',])
			->addColumn('verification', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '验证',])
			->addColumn('created_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建时间',])
			->addColumn('updated_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '更新时间',])
			->addColumn('deleted_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '软删除字段',])
			->addColumn('creator_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建人ID',])
            ->create();
    }
}
