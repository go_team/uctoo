<?php
namespace catchAdmin\wechatopen\controller;

use app\BaseController;
use catchAdmin\permissions\model\Users;
use catcher\CatchResponse;
use think\facade\Log;

class User extends BaseController
{
    protected $user;

    public function initialize()
    {
       $this->user = new Users;
    }

    /**
     *
     * @time 2021年09月15日
     * @param $id
     * @return \think\response\Json
     */
    public function read()
    {
        $id = input('id');
        $user = $this->user->findBy($id);
        $res = ['email'=>$user['email'],
                'username'=>$user['username'],
                'avatar'=>$user['avatar'],
                'remember_token'=>$user['remember_token'],
        ];
        return CatchResponse::success($res,'获取管理后台帐号成功');
    }

}
