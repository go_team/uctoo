<?php

namespace catchAdmin\wechatopen\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\wechatopen\model\Applet as appletModel;

class Wechatauth extends CatchController
{
    protected $appletModel;
    
    public function __construct(AppletModel $appletModel)
    {
        $this->appletModel = $appletModel;
    }
    
    /**
     * 列表
     * @time 2021年03月12日 19:46
     * @param Request $request 
     */
    public function index(Request $request) : \think\Response
    {
        return CatchResponse::paginate($this->appletModel->getList());
    }
    
    /**
     * 保存信息
     * @time 2021年03月12日 19:46
     * @param Request $request 
     */
    public function save(Request $request) : \think\Response
    {
        return CatchResponse::success($this->appletModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年03月12日 19:46
     * @param $id 
     */
    public function read($id) : \think\Response
    {
        return CatchResponse::success($this->appletModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年03月12日 19:46
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\Response
    {
        return CatchResponse::success($this->appletModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年03月12日 19:46
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->appletModel->deleteBy($id));
    }

    public static function getAuditStatusList(){
        return  [
            self::AUDIT_STATUS_SUCCESS => '审核成功',
            self::AUDIT_STATUS_FAIL => '审核被拒绝',
            self::AUDIT_STATUS_WAIT => '审核中',
            self::AUDIT_STATUS_REVOKE => '已撤回',
            self::AUDIT_STATUS_DELAY => '审核延后'
        ];
    }

    /**
     * 获取所有
     *
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getTypedata()
    {
        return CatchResponse::success($this->job->field(['id', 'job_name'])->select());
    }
}