<?php

namespace catchAdmin\wechatopen\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\wechatopen\model\AdminApplet as adminAppletModel;

class AdminApplet extends CatchController
{
    protected $adminAppletModel;
    
    public function __construct(AdminAppletModel $adminAppletModel)
    {
        $this->adminAppletModel = $adminAppletModel;
    }
    
    /**
     * 列表
     * @time 2021年03月14日 17:32
     * @param Request $request 
     */
    public function index(Request $request) : \think\Response
    {
        return CatchResponse::paginate($this->adminAppletModel->getList());
    }
    
    /**
     * 保存信息
     * @time 2021年03月14日 17:32
     * @param Request $request 
     */
    public function save(Request $request) : \think\Response
    {
        return CatchResponse::success($this->adminAppletModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年03月14日 17:32
     * @param $id 
     */
    public function read($id) : \think\Response
    {
        return CatchResponse::success($this->adminAppletModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年03月14日 17:32
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\Response
    {
        return CatchResponse::success($this->adminAppletModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年03月14日 17:32
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->adminAppletModel->deleteBy($id));
    }

    /**
     * 读取
     * @time 2021年03月14日 17:32
     * @param $creator_id
     */
    public function adminApplet($creator_id) : \think\Response
    {
        return CatchResponse::success($this->adminAppletModel->adminApplet($creator_id));
    }
}