<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: UCToo [ contact@uctoo.com ]
// +----------------------------------------------------------------------

use think\migration\Seeder;

class CodelabsTemplatesSeed extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     * generate by command php think create:seed codelabs_templates -m develop
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
       $data = array (
  0 => 
  array (
    'id' => 1,
    'name' => 'UCToo表格CURD',
    'template_code' => NULL,
    'filename' => 'elmform.stub',
    'filepath' => '/extend/catcher/command/stubs/elm/',
    'config_json' => NULL,
    'template_url' => '',
    'opensource' => 1,
    'author' => 'UCToo',
    'author_url' => 'https://www.uctoo.com',
    'user_id' => 1,
    'tags' => 'template',
    'docs' => '',
    'images' => '',
    'description' => 'headless CMS 默认CURD页面',
    'price' => 0,
    'status' => 1,
    'created_at' => 1635928574,
    'updated_at' => 1635928986,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
  1 => 
  array (
    'id' => 2,
    'name' => 'select',
    'template_code' => NULL,
    'filename' => 'select.vue',
    'filepath' => '/catch/element/view',
    'config_json' => '[{
  "interface": "input",
  "type": "string",
  "key": "v_model",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "v_model",
    "placeholder": "绑定的数据变量",
    "display": "none",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "placeholder",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "placeholder",
    "placeholder": "占位符",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "remote_method",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "remote_method",
    "placeholder": "查询数据的远程方法",
    "display": "none",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "optionList",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "optionList",
    "placeholder": "选项列表数据",
    "display": "none",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "key",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "key",
    "placeholder": "选项列表键名，应与查询接口返回数据主键一致",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "label",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "label",
    "placeholder": "界面显示项，使用查询接口返回数据中用于界面展示的字段",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "value",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "value",
    "placeholder": "选项列表存储值，使用查询接口返回数据实际赋值给select组件的字段值",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "tables",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "tables",
    "placeholder": "查询数据的远程接口，一般应与后台一个数据表查询接口地址对应",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "field",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "field",
    "placeholder": "查询数据的查询参数，一般应与后台一个数据表查询接口可支持的查询参数对应",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
}]',
    'template_url' => NULL,
    'opensource' => 1,
    'author' => 'UCToo',
    'author_url' => 'https://www.uctoo.com',
    'user_id' => 1,
    'tags' => 'components',
    'docs' => NULL,
    'images' => NULL,
    'description' => 'element select 组件',
    'price' => 0,
    'status' => 1,
    'created_at' => 1635928574,
    'updated_at' => 1635928986,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
  2 => 
  array (
    'id' => 3,
    'name' => 'input',
    'template_code' => NULL,
    'filename' => 'input.vue',
    'filepath' => '/catch/element/view',
    'config_json' => '[{
  "interface": "input",
  "type": "string",
  "key": "v_model",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "v_model",
    "placeholder": "绑定的数据变量",
    "display": "none",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "placeholder",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "placeholder",
    "placeholder": "占位符",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "type",
  "value": "text",
  "uiSchema": {
    "type": "string",
    "label": "type",
    "placeholder": "text，textarea 和其他 原生 input 的 type 值",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
}]',
    'template_url' => NULL,
    'opensource' => 1,
    'author' => 'UCToo',
    'author_url' => 'https://www.uctoo.com',
    'user_id' => 1,
    'tags' => 'components',
    'docs' => NULL,
    'images' => NULL,
    'description' => 'element input 组件',
    'price' => 0,
    'status' => 1,
    'created_at' => 1635928574,
    'updated_at' => 1635928986,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
  3 => 
  array (
    'id' => 4,
    'name' => 'upload',
    'template_code' => NULL,
    'filename' => 'upload.vue',
    'filepath' => '/catch/element/view',
    'config_json' => '[{
  "interface": "input",
  "type": "string",
  "key": "filetype",
  "value": "image",
  "uiSchema": {
    "type": "string",
    "label": "文件类型",
    "placeholder": "文件类型，仅支持image或file",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "count",
  "value": "1",
  "uiSchema": {
    "type": "string",
    "label": "count",
    "placeholder": "支持上传的文件数量",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "uploadtip",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "uploadtip",
    "placeholder": "上传提示信息",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
}]',
    'template_url' => NULL,
    'opensource' => 1,
    'author' => 'UCToo',
    'author_url' => 'https://www.uctoo.com',
    'user_id' => 1,
    'tags' => 'components',
    'docs' => NULL,
    'images' => NULL,
    'description' => 'element upload 组件',
    'price' => 0,
    'status' => 1,
    'created_at' => 1635928574,
    'updated_at' => 1635928986,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
  4 => 
  array (
    'id' => 5,
    'name' => 'datepicker',
    'template_code' => NULL,
    'filename' => 'datepicker.vue',
    'filepath' => '/catch/element/view',
    'config_json' => '[{
  "interface": "input",
  "type": "string",
  "key": "field",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "field",
    "placeholder": "绑定的数据变量",
    "display": "none",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "placeholder",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "placeholder",
    "placeholder": "占位符",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "type",
  "value": "date",
  "uiSchema": {
    "type": "string",
    "label": "type",
    "placeholder": "默认date，可选值year/month/date/dates/ week/datetime/datetimerange/ daterange/monthrange",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
}]',
    'template_url' => NULL,
    'opensource' => 1,
    'author' => 'UCToo',
    'author_url' => 'https://www.uctoo.com',
    'user_id' => 1,
    'tags' => 'components',
    'docs' => NULL,
    'images' => NULL,
    'description' => 'element datapicker 组件',
    'price' => 0,
    'status' => 1,
    'created_at' => 1635928574,
    'updated_at' => 1635928986,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
  5 => 
  array (
    'id' => 6,
    'name' => 'switch',
    'template_code' => NULL,
    'filename' => 'switch.vue',
    'filepath' => '/catch/element/view',
    'config_json' => '[{
  "interface": "input",
  "type": "string",
  "key": "field",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "field",
    "placeholder": "绑定的数据变量",
    "display": "none",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "active_text",
  "value": "开",
  "uiSchema": {
    "type": "string",
    "label": "active_text",
    "placeholder": "switch 打开时的文字描述",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "inactive_text",
  "value": "关",
  "uiSchema": {
    "type": "string",
    "label": "inactive_text",
    "placeholder": "switch 关闭时的文字描述",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "active_value",
  "value": "1",
  "uiSchema": {
    "type": "string",
    "label": "active_value",
    "placeholder": "switch 打开时的值",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "inactive_value",
  "value": "0",
  "uiSchema": {
    "type": "string",
    "label": "inactive_value",
    "placeholder": "switch 关闭时的值",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
}]',
    'template_url' => NULL,
    'opensource' => 1,
    'author' => 'UCToo',
    'author_url' => 'https://www.uctoo.com',
    'user_id' => 1,
    'tags' => 'components',
    'docs' => NULL,
    'images' => NULL,
    'description' => 'element switch 组件',
    'price' => 0,
    'status' => 1,
    'created_at' => 1635928574,
    'updated_at' => 1635928986,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
  6 => 
  array (
    'id' => 7,
    'name' => 'chinaareadata',
    'template_code' => NULL,
    'filename' => 'chinaareadata.vue',
    'filepath' => '/catch/element/view',
    'config_json' => '[{
  "interface": "input",
  "type": "string",
  "key": "field",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "field",
    "placeholder": "绑定的数据变量",
    "display": "none",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
}]',
    'template_url' => NULL,
    'opensource' => 1,
    'author' => 'UCToo',
    'author_url' => 'https://github.com/Plortinus',
    'user_id' => 1,
    'tags' => 'components',
    'docs' => NULL,
    'images' => NULL,
    'description' => 'element china area data组件',
    'price' => 0,
    'status' => 1,
    'created_at' => 1635928574,
    'updated_at' => 1635928986,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
);

        foreach ($data as $item) {
            \catchAdmin\develop\model\CodelabsTemplates::create($item);
        }
    }
}