<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: UCToo [ contact@uctoo.com ]
// +----------------------------------------------------------------------

use think\migration\Seeder;

class VueEditorPagesSeed extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
       $data = array (
  0 => 
  array (
    'id' => 1,
    'name' => '商城首页',
    'type' => 'shop',
    'image' => NULL,
    'memo' => NULL,
    'platform' => 'PC',
    'page_title' => '首页',
    'page_path' => NULL,
    'user_id' => 1,
    'appstore_user_id' => 1,
    'status' => 0,
    'edit_status' => 1,
    'tags' => 'lowcode',
    'description' => NULL,
    'created_at' => 0,
    'updated_at' => 0,
    'deleted_at' => 0,
    'creator_id' => 0,
  ),
  1 => 
  array (
    'id' => 2,
    'name' => 'APP首页',
    'type' => 'shop',
    'image' => NULL,
    'memo' => NULL,
    'platform' => 'PC',
    'page_title' => '首页',
    'page_path' => NULL,
    'user_id' => 1,
    'appstore_user_id' => 1,
    'status' => 0,
    'edit_status' => 0,
    'tags' => 'lowcode',
    'description' => NULL,
    'created_at' => 0,
    'updated_at' => 0,
    'deleted_at' => 0,
    'creator_id' => 0,
  ),
);

        foreach ($data as $item) {
            \catchAdmin\develop\model\VueEditorPages::create($item);
        }
    }
}