<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2022 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: UCToo <contact@uctoo.com>
// +----------------------------------------------------------------------
namespace catchAdmin\develop\model;

use catcher\base\CatchModel as Model;
/**
 *
 * @property int $id
 * @property string $name
 * @property string $template_code
 * @property string $filename
 * @property string $filepath
 * @property string $config_json
 * @property string $template_url
 * @property int $opensource
 * @property string $author
 * @property string $author_url
 * @property int $user_id
 * @property string $tags
 * @property string $docs
 * @property string $images
 * @property string $description
 * @property int $price
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class CodelabsTemplates extends Model
{
    
    public $field = [
        //
        'id',
        // 模板名称
        'name',
        // 模板代码
        'template_code',
        // 模板文件名
        'filename',
        // 模板路径
        'filepath',
        // 模板配置项
        'config_json',
        // 模板url地址
        'template_url',
        // 是否开源
        'opensource',
        // 作者
        'author',
        // 作者url
        'author_url',
        // 关联users表id
        'user_id',
        // 模板标签
        'tags',
        // 模板文档
        'docs',
        // 模板图片
        'images',
        // 模板描述
        'description',
        // 模板标价
        'price',
        // 模板状态
        'status',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    ];
    
    public $name = 'codelabs_templates';
}