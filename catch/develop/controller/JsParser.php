<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2022 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: UCToo <contact@uctoo.com>
// +----------------------------------------------------------------------
namespace catchAdmin\develop\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use think\Response;
use Peast\Peast;
use uctoo\library\JsParser as utilJsParser;

class JsParser extends CatchController
{
    /**
     * @time 2021/11/25 12:32
     * @return Response
     */
    public function index(): Response
    {
        $string = input('string');
        $options = array(
            "sourceType" => Peast::SOURCE_TYPE_MODULE,
            "comments" => true
        );
        $ast = Peast::latest($string, $options)->parse();
        $token = Peast::latest($string, $options)->tokenize();

        $result = ['ast' => $ast, 'token' => $token];
        return CatchResponse::success($result);
    }

    /**
     * @time 2021/11/25 12:32
     * @return Response
     */
    public function js_merge(): Response
    {
        $string1 = input('string1');
        $string2 = input('string2');
        $result = utilJsParser::js_merge($string1,$string2);

        return CatchResponse::success($result);
    }

}