<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------

// minishopSpu路由
$router->group(function () use ($router){
    $router->resource('minishopSpu', '\catchAdmin\minishop\controller\MinishopSpu');
})->middleware('auth');
$router->post('minishop/product/category/sync', '\catchAdmin\minishop\controller\MinishopProductCategory@sync');