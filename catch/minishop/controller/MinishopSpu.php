<?php

namespace catchAdmin\minishop\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\minishop\model\MinishopSpu as MinishopSpuModel;

class MinishopSpu extends CatchController
{
    protected $MinishopSpuModel;
    
    public function __construct(MinishopSpuModel $MinishopSpuModel)
    {
        $this->MinishopSpuModel = $MinishopSpuModel;
    }
    
    /**
     * 列表
     * @time 2021年05月13日 19:05
     * @param Request $request 
     */
    public function index(Request $request) : \think\response\Json
    {
        return CatchResponse::success($this->MinishopSpuModel->getList());
    }
    
    /**
     * 保存信息
     * @time 2021年05月13日 19:05
     * @param Request $request 
     */
    public function save(Request $request) : \think\response\Json
    {
        return CatchResponse::success($this->MinishopSpuModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年05月13日 19:05
     * @param $id 
     */
    public function read($id) : \think\response\Json
    {
        return CatchResponse::success($this->MinishopSpuModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年05月13日 19:05
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\response\Json
    {
        return CatchResponse::success($this->MinishopSpuModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年05月13日 19:05
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->MinishopSpuModel->deleteBy($id));
    }
}