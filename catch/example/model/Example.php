<?php

namespace catchAdmin\example\model;

use catcher\base\CatchModel as Model;

class Example extends Model
{
    // 表名
    public $name = 'example';
    // 数据库字段映射
    public $field = array(
        'id',
        // 模块名称
        'module',
        // 操作模块
        'operate',
        // 路由
        'route',
        // 参数
        'params',
        // ip
        'ip',
        // 请求方法
        'method',
        // 创建人ID
        'creator_id',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除
        'deleted_at',
    );
}