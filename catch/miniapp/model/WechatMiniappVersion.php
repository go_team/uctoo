<?php

namespace catchAdmin\miniapp\model;

use catcher\base\CatchModel as Model;

class WechatMiniappVersion extends Model
{
    // 表名
    public $name = 'wechat_miniapp_version';
    // 数据库字段映射
    public $field = array(
        'id',
	// appid
        'appid',
        // SaaS产品ID
        'product_id',
        // 第三方平台代码库中的代码模版ID
        'template_id',
        // 第三方模板自定义的配置
        'ext_json',
        // 代码版本号，开发者可自定义
        'user_version',
        // 代码描述，开发者可自定义
        'user_desc',
        // 代码状态:-1=已下线,0=未上传,1=已上传,2=已发布
        'status',
        // 审核状态:-1=未提交审核,0=审核成功,1=审核失败,2=审核中
        'audit_status',
        // 可填选的类目列表
        'category_list',
        // 页面配置列表
        'page_list',
        // 提交审核项的一个列表（至少填写1项，至多填写5项）
        'item_list',
        // 提交审核时获得的审核id
        'auditid',
        // 审核不通过原因
        'reason',
        // 审核成功时间
        'succ_time',
        // 审核失败时间
        'fail_time',
        // 创建人ID
        'creator_id',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除
        'deleted_at',
    );
}