<?php

use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class WxaUpdatableMessage extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('wxa_updatable_message', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信小程序私密消息' ,'id' => 'id' ,'primary_key' => ['id']]);
        $table->addColumn('user_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => '小程序用户表id',])
			->addColumn('openid', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => 'openid',])
			->addColumn('unionid', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => 'unionid',])
			->addColumn('activity_id', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '动态消息的 ID',])
			->addColumn('expiration_time', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => 'activity_id 的过期时间戳。默认24小时后过期。',])
			->addColumn('valid', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => '验证是否通过',])
			->addColumn('iv', 'string', ['limit' => 512,'null' => true,'signed' => true,'comment' => '加密算法的初始向量，详细见加密数据解密算法',])
			->addColumn('encryptedData', 'string', ['limit' => 512,'null' => true,'signed' => true,'comment' => '经过加密的activityId，解密后可得到原始的activityId',])
			->addColumn('shareTicket', 'string', ['limit' => 512,'null' => true,'signed' => true,'comment' => 'shareTicket',])
			->addColumn('member_count', 'string', ['limit' => 512,'null' => true,'signed' => true,'comment' => '状态 0 时有效，文字内容模板中 member_count 的值',])
			->addColumn('room_limit', 'string', ['limit' => 512,'null' => true,'signed' => true,'comment' => '状态 0 时有效，文字内容模板中 room_limit 的值',])
			->addColumn('path', 'string', ['limit' => 512,'null' => true,'signed' => true,'comment' => '状态 1 时有效，点击「进入」启动小程序时使用的路径。',])
			->addColumn('version_type', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '状态1时有效，点击「进入」启动小程序时使用的版本。有效参数值为：develop（开发版），trial（体验版），release（正式版）',])
			->addColumn('target_state', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => '动态消息修改后的状态。0=未开始，1=已开始',])
			->addColumn('template_info', 'string', ['limit' => 512,'null' => true,'signed' => true,'comment' => '动态消息对应的模板信息',])
			->addColumn('to_openid', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '接收人openid',])
			->addColumn('created_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建时间',])
			->addColumn('updated_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '更新时间',])
			->addColumn('deleted_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '软删除字段',])
			->addColumn('creator_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建人ID',])
			->addIndex(['activity_id'], ['name' => 'wxa_updatable_message_activity_id'])
			->addIndex(['openid'], ['name' => 'wxa_updatable_message_openid'])
			->addIndex(['user_id'], ['name' => 'wxa_updatable_message_user_id'])
            ->create();
    }
}
