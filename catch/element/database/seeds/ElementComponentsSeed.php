<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: UCToo [ contact@uctoo.com ]
// +----------------------------------------------------------------------

use think\migration\Seeder;

class ElementComponentsSeed extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
       $data = array (
  0 => 
  array (
    'id' => 1,
    'name' => 'select',
    'tag' => 'el-select',
    'docs_url' => 'https://element.eleme.cn/#/zh-CN/component/select',
    'specification' => '{}',
    'version' => 'v2.15.7',
    'remarks' => NULL,
    'created_at' => 1637118882,
    'updated_at' => 1637118882,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
  1 => 
  array (
    'id' => 2,
    'name' => 'input',
    'tag' => 'el-input',
    'docs_url' => 'https://element.eleme.cn/#/zh-CN/component/input',
    'specification' => '{}',
    'version' => 'v2.15.7',
    'remarks' => NULL,
    'created_at' => 1637118882,
    'updated_at' => 1637118882,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
  2 => 
  array (
    'id' => 3,
    'name' => 'upload',
    'tag' => 'el-upload',
    'docs_url' => 'https://element.eleme.cn/#/zh-CN/component/upload',
    'specification' => '{}',
    'version' => 'v2.15.7',
    'remarks' => NULL,
    'created_at' => 1637118882,
    'updated_at' => 1637118882,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
);

        foreach ($data as $item) {
            \catchAdmin\element\model\ElementComponents::create($item);
        }
    }
}