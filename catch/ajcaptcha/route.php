<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------

// you should use `$router`
/* @var think\Route $router */

$router->group(function () use ($router){
})->middleware('auth');

$router->post('/captcha/get', 'catchAdmin\ajcaptcha\controller\Index@index')->allowCrossDomain();
$router->post('/captcha/check', 'catchAdmin\ajcaptcha\controller\Index@check')->allowCrossDomain();
$router->post('/captcha/verification', 'catchAdmin\ajcaptcha\controller\Index@verification')->allowCrossDomain();