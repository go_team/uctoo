<?php

namespace catchAdmin\sms\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\sms\model\SmsLog as SmsLogModel;
use catcher\Code;

class SmsLog extends CatchController
{

    protected $SmsLogModel;

    public function __construct(SmsLogModel $SmsLogModel)
    {
        $this->SmsLogModel = $SmsLogModel;
    }
    
    /**
     * 列表
     * @time 2021年07月02日 12:39
     * @param Request $request 
     */
    public function index(Request $request) : \think\Response
    {
        return CatchResponse::paginate($this->SmsLogModel->getList());
    }
    
    /**
     * 保存信息
     * @time 2021年07月02日 12:39
     * @param Request $request 
     */
    public function save(Request $request) : \think\Response
    {
        return CatchResponse::success($this->SmsLogModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年07月02日 12:39
     * @param $id 
     */
    public function read($id) : \think\Response
    {
        return CatchResponse::success($this->SmsLogModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年07月02日 12:39
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\Response
    {
        return CatchResponse::success($this->SmsLogModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年07月02日 12:39
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->SmsLogModel->deleteBy($id));
    }

}