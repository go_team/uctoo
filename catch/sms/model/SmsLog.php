<?php

namespace catchAdmin\sms\model;

use catcher\base\CatchModel as Model;
use think\facade\Log;
use think\facade\Db;
/**
 *
 * @property int $id
 * @property string $event
 * @property string $mobile
 * @property string $code
 * @property int $times
 * @property string $ip
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class SmsLog extends Model
{
    // 表名
    public $name = 'sms_log';
    // 数据库字段映射
    public $field = array(
        'id',
        // 事件
        'event',
        // 手机号
        'mobile',
        // 验证码
        'code',
        // 验证次数
        'times',
        // IP
        'ip',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    );

    /**
     * 验证码有效时长
     * @var int
     */
    protected static $expire = 300;
    /**
     * 最大允许检测的次数
     * @var int
     */
    protected static $maxCheckNums = 10;

    /**
     * 校验验证码
     *
     * @param int $mobile 手机号
     * @param int $code 验证码
     * @param string $event 事件
     * @return  boolean
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function check($mobile, $code, $event = 'default')
    {
        $time = time() - self::$expire;
        $sms = Db::table('sms_log')->where(['mobile' => $mobile, 'event' => $event])  //获取原始值
            ->order('id', 'desc')
            ->find();
        if ($sms) {
            if ($sms['created_at'] > $time && $sms['times'] <= self::$maxCheckNums) {
                if($code == $sms['code']) {
                    return true;
                }else{
                    Db::table('sms_log')->where(['mobile' => $mobile, 'event' => $event])->inc('times')->update();
                    return false;
                }
            } else {
                // 过期则清空该手机验证码
                self::flush($mobile, $event);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 清空指定手机号验证码
     *
     * @param int $mobile 手机号
     * @param string $event 事件
     * @return  boolean
     * @throws \Exception
     */
    public static function flush($mobile, $event = 'default')
    {
        self::where(['mobile' => $mobile, 'event' => $event])
            ->delete();
        return true;
    }
}