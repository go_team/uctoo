<?php
namespace catchAdmin\appstore\tables;


use catcher\CatchTable;
use catchAdmin\appstore\tables\forms\Factory;
use catcher\library\table\Actions;
use catcher\library\table\HeaderItem;
use catcher\library\table\Search;

class Management extends CatchTable
{
    public function table()
    {
        return $this->getTable('management')
                    ->header([
                        HeaderItem::label('')->type('selection'),
                        HeaderItem::label('product_id')->prop('product_id'),
                        HeaderItem::label('out_product_id')->prop('out_product_id'),
                        HeaderItem::label('标题')->prop('title'),
                        HeaderItem::label('template_id')->prop('template_id'),
                        HeaderItem::label('销量')->prop('sales_count'),
                        HeaderItem::label('状态')->prop('status')->withSwitchComponent(),
                        HeaderItem::label('创建时间')->prop('created_at'),
                        HeaderItem::label('操作')->width(250)->actions([
                            Actions::update(),
                            Actions::delete()
                        ])
                    ])
                    ->withActions([
                      Actions::create()
                    ])
                    ->withSearch([
                        Search::label('标题')->text('title', '标题')
                    ])
                    ->withApiRoute('appstore')
                    ->selectionChange()
                    ->render();
    }


    public function form()
    {
        // TODO: Implement form() method.
        return Factory::create('management');
    }
}