<?php

namespace catchAdmin\permissions\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\permissions\model\RoleHasGroup as RoleHasGroupModel;
use think\Response;

class RoleHasGroup extends CatchController
{
    
    protected $roleHasGroupModel;
    
    /**
     *
     * @time 2022/02/23 21:16
     * @param RoleHasGroupModel $roleHasGroupModel
     * @return mixed
     */
    public function __construct(RoleHasGroupModel $roleHasGroupModel)
    {
        $this->roleHasGroupModel = $roleHasGroupModel;
    }
    
    /**
     *
     * @time 2022/02/23 21:16
     * @return Response
     */
    public function index() : Response
    {
        return CatchResponse::paginate($this->roleHasGroupModel->getList());
    }
    
    /**
     *
     * @time 2022/02/23 21:16
     * @param Request $request
     * @return Response
     */
    public function save(Request $request) : Response
    {
        return CatchResponse::success($this->roleHasGroupModel->storeBy($request->post()));
    }
    
    /**
     *
     * @time 2022/02/23 21:16
     * @param $id
     * @return Response
     */
    public function read($id) : Response
    {
        return CatchResponse::success($this->roleHasGroupModel->findBy($id));
    }
    
    /**
     *
     * @time 2022/02/23 21:16
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request) : Response
    {
        return CatchResponse::success($this->roleHasGroupModel->updateBy($id, $request->post()));
    }
    
    /**
     *
     * @time 2022/02/23 21:16
     * @param $id
     * @return Response
     */
    public function delete($id) : Response
    {
        return CatchResponse::success($this->roleHasGroupModel->deleteBy($id));
    }
}