<?php

namespace catchAdmin\permissions\controller;

use catchAdmin\wechatopen\model\WechatopenMiniappUsers;
use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\permissions\model\UsersGroup as UsersGroupModel;
use catcher\Code;
use think\facade\Log;
use think\Response;

class UsersGroup extends CatchController
{
    
    protected $usersGroupModel;
    
    /**
     *
     * @time 2022/02/21 19:12
     * @param UsersGroupModel $usersGroupModel
     * @return mixed
     */
    public function __construct(UsersGroupModel $usersGroupModel)
    {
        $this->usersGroupModel = $usersGroupModel;
    }
    
    /**
     *
     * @time 2022/02/21 19:12
     * @return Response
     */
    public function index() : Response
    {
        return CatchResponse::success($this->usersGroupModel->getList());
    }
    
    /**
     *
     * @time 2022/02/21 19:12
     * @param Request $request
     * @return Response
     */
    public function save(Request $request) : Response
    {
        return CatchResponse::success($this->usersGroupModel->storeBy($request->post()));
    }
    
    /**
     *
     * @time 2022/02/21 19:12
     * @param $id
     * @return Response
     */
    public function read($id) : Response
    {
        return CatchResponse::success($this->usersGroupModel->findBy($id));
    }
    
    /**
     *
     * @time 2022/02/21 19:12
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request) : Response
    {
        return CatchResponse::success($this->usersGroupModel->updateBy($id, $request->post()));
    }
    
    /**
     *
     * @time 2022/02/21 19:12
     * @param $id
     * @return Response
     */
    public function delete($id) : Response
    {
        return CatchResponse::success($this->usersGroupModel->deleteBy($id));
    }



    /**
     * 用户组包含的用户。可以支持一个组中保存多态用户，但是建议一个组中只保存同一模型的用户。
     *
     * @time 2022年02月22日
     * @param \think\Request $request
     * @param int id 用户组表id
     * @param string morphModel 多态用户表完整类名，例如 catchAdmin\wechatopen\model\WechatopenMiniappUsers 对应 user_has_group 表 groupable_type字段。可支持不传此参数，返回所有多态用户。
     * @return \think\response\Json
     */
    public function getUsers(\think\Request $request)
    {
        $id = input('id');
        $group = $this->usersGroupModel->findBy($id);
        $morphModel = input('morphModel');
        if(isset($morphModel)){
            $result = $group->groupable()->select()->toArray();
        }else{
            $res = $group->groupable();
            $result = [];
            foreach ($res as $key => $morph){
                 $result[$key] = $morph->select()->toArray();
            }
        }
        return CatchResponse::success($result);
    }

    /**
     * 用户组包含的小程序用户。可以支持一个组中保存多态用户，但是建议一个组中只保存同一模型的用户。
     *
     * @time 2022年02月22日
     * @param \think\Request $request
     * @param int id 用户组表id，高优先级获取用户组
     * @param string token 用户唯一标识，低优先级获取用户组。如未传入用户组表id,即默认获取当前用户所创建组中的用户
     * @param string morphModel 多态用户表完整类名，例如 catchAdmin\wechatopen\model\WechatopenMiniappUsers 对应 user_has_group 表 groupable_type字段。可支持不传此参数，返回所有多态用户。
     * @return \think\response\Json
     */
    public function getMiniappUsers(\think\Request $request)
    {
        $id = input('id');
        $token = $request->header('token');
        if(isset($id)){
            $group = $this->usersGroupModel->findBy($id);
        }elseif (isset($token)){
            $user = WechatopenMiniappUsers::where('token',$token)->find();
            $group = $this->usersGroupModel->where('code','=',$user->openid)->find();
        }
        if(!$group){
            return CatchResponse::fail('无有效用户组', Code::PARAM_ERROR);
        }

        $morphModel = input('morphModel');
        if(isset($morphModel)){
            $result = $group->groupable()->select()->toArray();
        }else{
            $res = $group->groupable();
            $result = [];
            foreach ($res as $key => $morph){
                $result[$key] = $morph->select()->toArray();
            }
        }
        return CatchResponse::success($result);
    }

    /**
     * 用户组关联的角色。可以支持一个组关联多态角色，但是建议一个组中只关联同一模型的角色。
     *
     * @time 2022年02月22日
     * @param \think\Request $request
     * @param int id 用户组表id
     * @param string morphModel 多态用户表完整类名，例如 catchAdmin\permissions\model\Roles 对应 role_has_group 表 groupable_type字段。可支持不传此参数，返回所有多态角色。
     * @return \think\response\Json
     */
    public function getRoles(\think\Request $request)
    {
        $id = input('id');
        $group = $this->usersGroupModel->findBy($id);
        $morphModel = input('morphModel');
        if(isset($morphModel)){
            $result = $group->rolegroupable()->select()->toArray();
        }else{
            $res = $group->rolegroupable();
            $result = [];
            foreach ($res as $key => $morph){
                $result[$key] = $morph->select()->toArray();
            }
        }
        return CatchResponse::success($result);
    }

    /**
     * 以小程序用户唯一标识openid初始化一个用户组。
     *
     * @time 2022年02月22日
     * @param \think\Request $request
     * @param string token 用户token
     * @return \think\response\Json 用户组数据
     */
    public function initMiniappUserGroup(\think\Request $request)
    {
        $token = $request->header('token');
        $group = $this->usersGroupModel->initMiniappUserGroup($token);

        return CatchResponse::success($group);
    }
}