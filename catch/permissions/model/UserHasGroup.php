<?php

namespace catchAdmin\permissions\model;


use catcher\base\CatchModel as Model;
/**
 *
 * @property int $id
 * @property int $groupable_id
 * @property string $groupable_type
 * @property int $group_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class UserHasGroup extends Model
{
    public $field = [
        //
        'id',
        // 多态用户ID
        'groupable_id',
        // 多态模型类型
        'groupable_type',
        // 用户组ID
        'group_id',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    ];

    public $name = 'user_has_group';

}