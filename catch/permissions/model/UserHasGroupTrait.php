<?php
declare(strict_types=1);

/**
 * 在需要支持多态分组的用户模型中使用此trait
 * @filename  UserHasGroup.php
 * @createdAt 2022/2/22
 * @project  https://gitee.com/uctoo/uctoo
 * @document https://gitee.com/uctoo/uctoo
 * @author   UCToo <contact@uctoo.com>
 * @copyright By UCToo
 * @license  https://gitee.com/uctoo/uctoo/raw/master/license.txt
 */
namespace catchAdmin\permissions\model;


trait UserHasGroupTrait
{
    /**
     *
     * @time 2019年12月08日
     * @return mixed
     */
    public function groups()
    {
        return $this->morphToMany(UsersGroup::class, 'user_has_group', 'groupable', 'group_id');
    }

    /**
     *
     * @time 2019年12月08日
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups()->select();
    }

    /**
     *
     * @time 2019年12月08日
     * @param array $departments
     * @return mixed
     */
    public function attachGroups(array $groups)
    {
        if (empty($groups)) {
            return true;
        }

        sort($groups);

        return $this->groups()->attach($groups);
    }

    /**
     *
     * @time 2019年12月08日
     * @param array $departments
     * @return mixed
     */
    public function detachGroups(array $groups = [])
    {
        if (empty($groups)) {
            return $this->groups()->detach();
        }

        return $this->groups()->detach($groups);
    }
}