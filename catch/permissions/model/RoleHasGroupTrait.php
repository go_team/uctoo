<?php

namespace catchAdmin\permissions\model;


trait RoleHasGroupTrait
{
    /**
     *
     * @time 2019年12月08日
     * @return mixed
     */
    public function groups()
    {
        return $this->morphToMany(UsersGroup::class, 'role_has_group', 'groupable', 'group_id');
    }

    /**
     *
     * @time 2019年12月08日
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups()->select();
    }

    /**
     *
     * @time 2019年12月08日
     * @param array $departments
     * @return mixed
     */
    public function attachGroups(array $groups)
    {
        if (empty($groups)) {
            return true;
        }

        sort($groups);

        return $this->groups()->attach($groups);
    }

    /**
     *
     * @time 2019年12月08日
     * @param array $departments
     * @return mixed
     */
    public function detachGroups(array $groups = [])
    {
        if (empty($groups)) {
            return $this->groups()->detach();
        }

        return $this->groups()->detach($groups);
    }
}