<?php
namespace catchAdmin\permissions\model\search;

trait UsersGroupSearch
{
    public function searchGroupNameAttr($query, $value, $data)
    {
        return $query->whereLike('group_name', $value);
    }

    public function searchCodeAttr($query, $value, $data)
    {
        return $query->where('code', $value);
    }
}
