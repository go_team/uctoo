permissions 模块是一个用于管理用户、用户组、角色、部门、权限数据、菜单以及之间关联关系的模块，是产品基础依赖模块。

# 概述

  本模块的设计目标是提供开发人员、产品人员等相关角色，可以管理系统中的三户模型、权限、菜单等相关数据。
  
## 主要特性
  


## 产品架构

## 安装教程
  
### 运行环境依赖

    
### 分步骤安装

## 使用手册

## 开发说明
### 多态用户组说明  
  表结构设计如下
  <table>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo_permissions/users_group.png"></td>
      </tr>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo_permissions/user_has_group.png"></td>
      </tr>
  </table>
  
1. 多态用户组支持使用一个用户组表users_group对多个不同模型（数据表）的用户进行分组的操作。在user_has_group表中保存多态用户与用户组的关联关系。  
2. 在需要支持多态分组的用户模型中使用catchAdmin\permissions\model\UserHasGroupTrait ,同时在需要支持多态分组方法的控制器中使用 catcher\traits\UserHasGroupTrait，
  控制器中必须有user成员变量，且user所赋值的模型需支持UserHasGroupTrait模型，在route.php中添加addGroups、getGroups、delGroups对应的路由，并且注意接口权限
3. 可先在管理后台用户组管理（或通过接口调用）在users_group表中添加用户组数据，再通过管理后台用户组关联（或通过addGroups、getGroups、delGroups接口调用）增删查用户与用户组的关联关系。
4. 以小程序用户组场景，示例一种使用方式：  
4.1 小程序端调用 userGroup/initMiniappUserGroup 接口返回（无则生成）一个小程序用户组，小程序端保存返回的用户组id待后续业务逻辑使用。
4.2 小程序端调用 userGroup/getMiniappUsers 接口查询用户组中的用户数据列表。用户列表页可调用 wechatopenMiniappUser/delGroups 接口将用户移出用户组。  
4.3 小程序分享页加入用户组id等参数分享给微信好友，微信好友打开分享页，调用 wechatopenMiniappUser/addGroups 接口，加入分享人的用户组。  
4.4 小程序端可通过判断微信好友是否在特定用户组中实现一定的业务逻辑。

### 多态角色组说明  
1. 与多态用户组使用逻辑完全相同。

### 模块roadmap
1. 支持多态用户组权限管理，用户的权限等于单独分配给用户的role权限，与用户所在用户组所分配的role权限合集。