<?php
/* @var think\Route $router */
$router->group(function () use($router) {
    // 角色
    $router->resource('roles', '\\catchAdmin\\permissions\\controller\\Role');
    $router->get('role/permissions/<id>', '\\catchAdmin\\permissions\\controller\\Role@getPermissions');
    // 权限
    $router->resource('permissions', '\\catchAdmin\\permissions\\controller\\Permission');
    $router->put('permissions/show/<id>', '\\catchAdmin\\permissions\\controller\\Permission@show');
    // 部门
    $router->resource('departments', '\\catchAdmin\\permissions\\controller\\Department');
    // 所有职位
    $router->get('jobs/all', '\\catchAdmin\\permissions\\controller\\Job@getAll');
    // 岗位
    $router->resource('jobs', '\\catchAdmin\\permissions\\controller\\Job');
    // 用户
    $router->resource('users', '\\catchAdmin\\permissions\\controller\\User');
    // 切换状态
    $router->put('users/switch/status/<id>', '\\catchAdmin\\permissions\\controller\\User@switchStatus');
    $router->put('user/profile', '\\catchAdmin\\permissions\\controller\\User@profile');
    $router->get('user/info', '\\catchAdmin\\permissions\\controller\\User@info');
    $router->get('user/export', '\\catchAdmin\\permissions\\controller\\User@export');
    // usersGroup 路由
    $router->resource('usersGroup', catchAdmin\permissions\controller\UsersGroup::class);
    // userHasGroup 路由
    $router->resource('userHasGroup', catchAdmin\permissions\controller\UserHasGroup::class);
    $router->post('user/addGroups', '\\catchAdmin\\permissions\\controller\\User@addGroups');
    $router->post('user/getGroups', '\\catchAdmin\\permissions\\controller\\User@getGroups');
    $router->post('user/delGroups', '\\catchAdmin\\permissions\\controller\\User@delGroups');
    $router->post('userGroup/getUsers', '\\catchAdmin\\permissions\\controller\\UsersGroup@getUsers');
    // roleHasGroup 路由
     $router->resource('roleHasGroup', catchAdmin\permissions\controller\RoleHasGroup::class);
    $router->post('role/addGroups', '\\catchAdmin\\permissions\\controller\\Role@addGroups');
    $router->post('role/getGroups', '\\catchAdmin\\permissions\\controller\\Role@getGroups');
    $router->post('role/delGroups', '\\catchAdmin\\permissions\\controller\\Role@delGroups');
    $router->post('userGroup/getRoles', '\\catchAdmin\\permissions\\controller\\UsersGroup@getRoles');
})->middleware('auth');
$router->post('userGroup/initMiniappUserGroup', '\\catchAdmin\\permissions\\controller\\UsersGroup@initMiniappUserGroup')->middleware(\uctoo\middleware\AuthWechatTokenMiddleware::class);
$router->post('userGroup/getMiniappUsers', '\\catchAdmin\\permissions\\controller\\UsersGroup@getMiniappUsers')->middleware(\uctoo\middleware\AuthWechatTokenMiddleware::class);