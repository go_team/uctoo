test 模块是一个用于测试、体验代码生成、模板生成等开发基础功能的模块。

# 概述

  本模块的设计目标是提供开发人员、产品人员等相关角色，可以快速掌握框架开发方法。
  
## 主要特性

1. 代码生成模块可以用于创建模块，可以通过定义数据表结构和字段，一键生成数据库表、创建控制器、创建模型、创建表迁移、创建CRUD表单页面、创建菜单。
2. 可以复用模板动态生成页面源码，适用于有规律的管理后台模板或者移动端模板，需要创建大量类似页面的应用场景。
3. 可以实现无状态数据、接口、文件等的可视化管理，基本可以与云开发CMS等产品相同效果。对于无状态的内容和服务，前端开发人员可以无需接触和了解后台代码。

## 产品架构
1. 可复用的页面模板文件位于 extend/catcher/command/stubs/elm 目录下。elmform.stub模板用于通过表结构生成默认的管理后台crud.vue页面。routerjs.stub模板用于生成模块router.js文件。
2. 用于生成crud.vue页面和router.js的命令行工具位于extend/catcher/command/Tools/CreateElmformCommand.php文件。命令行需要先在config/console.php文件中注册才可以在框架中任意位置通过代码执行命令行工具（其实在代码中动态注册也可以）。
3. 页面生成命令行中采用了querylist（http://www.querylist.cc/）动态处理模板，querylist的DOM操作方法和API与jquery基本完全相同。
4. 生成的crud.vue页面位于模块目录下views目录内，子目录结构与vue-element的views目录结构相同，可以手动合并到对应的前端项目开发目录。目录结构遵循 模块名/数据表名/crud.vue 的默认约定。

## 安装教程
1. 可手动合并代码生成模块开源项目至本地开发环境使用。后台项目源码 gitee.com/uctoo/uctoo ,前端vue项目需更新 views/system/generate/index.vue 文件，源码地址 https://gitee.com/UCT/uctoo-app-server-vue 
2. 可通过gitee.com/uctoo/uctoo 开源项目一键部署公有云使用环境进行使用。生成的代码在CFS云存储中长期保存，可同步至本地开发环境进行二次开发。
3. 由于代码生成属于开发阶段的工具，可能会对运营环境产生不可预知的影响，因此不建议在运营环境开放代码生成功能提供用户使用。demo.uctoo.com 测试环境仅提供体验界面，无执行权限。

### 运行环境依赖
1. 使用页面生成功能需先安装 composer require jaeger/querylist
2. 建议PHP版本大于7.4

## 使用手册
  演示地址：demo.uctoo.com 控制台使用demo帐号登录
  模块使用界面截图：
  <table>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/codegenerate.png"></td>
      </tr>
  </table>

## 开发说明  

1. 默认生成的后台代码和前端代码主要用于对元数据的增删改查，属于无状态的接口，也主要提供管理员使用，不建议进行修改。有状态的接口和页面，可以参考自动生成的代码新增开发。
2. 用QueryList处理模板,会自动把自闭合标签都处理成闭合标签，例如<input />输出成<input></input>，对功能没影响。
3. 由于QueryList无法处理 @click、@close等@开头的标签事件，因此设计模板时所有@都用el-event-at_ 人工替换，动态生成页面后，再程序替换回 @。
4. 页面文件的name属性需要遵循 模块名_控制器名 （即与router.js中的前端路由定义一致），页面的keepalive状态才有效。
5. 本代码生成模块主要提供一种动态页面生成方法，不同模板对应的页面生成命令算法也不同，需要开发者自行根据不同的可复用模板编写动态生成算法。
6. 本开源版本代码生成模块仅使用了el-input组件，开发者可以自行在页面生成命令中根据inputType生成其他可复用组件。
            
代码生成页提交到后台generate接口的数据结构
array (
  'controller' => 
  array (
    'module' => 'test',
    'controller' => 'catchAdmin\\test\\controller\\test',
    'table' => 'test',
    'model' => 'catchAdmin\\test\\model\\test',
    'restful' => true,
  ),
  'table_fields' => 
  array (
    0 => 
    array (
      'field' => 'module',
      'type' => 'varchar',
      'length' => 50,
      'nullable' => false,
      'index' => '',
      'default' => '',
      'comment' => '模块',
      'unsigned' => false,
      'inputType' => 'input',
    ),
    1 => 
    array (
      'field' => 'operate',
      'type' => 'varchar',
      'length' => 20,
      'nullable' => false,
      'index' => '',
      'default' => '',
      'comment' => '操作',
      'unsigned' => false,
      'inputType' => 'textarea',
    ),
    2 => 
    array (
      'field' => 'route',
      'type' => 'varchar',
      'length' => 100,
      'nullable' => false,
      'index' => '',
      'default' => '',
      'comment' => '路由',
      'unsigned' => false,
      'inputType' => 'input',
    ),
  ),
  'table_extra' => 
  array (
    'primary_key' => 'id',
    'created_at' => true,
    'soft_delete' => true,
    'creator_id' => true,
    'engine' => 'InnoDB',
    'comment' => '测试',
  ),
  'create_controller' => true,
  'create_model' => true,
  'create_migration' => true,
  'create_table' => true,
  'create_crudform' => true,
)

### 自动生成权限菜单
自动生成菜单的 permissions 表新增数据示例  
INSERT INTO `数据库名`.`permissions` (`id`, `permission_name`, `parent_id`, `level`, `route`, `icon`, `module`, `creator_id`, `permission_mark`, `component`, `redirect`, `keepalive`, `type`, `hidden`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES ('262', 'test', '0', '1', '/test', '', 'test', '1', 'test', 'layout', '', '1', '1', '1', '0', '1634613508', '1634613508', '0');
INSERT INTO `数据库名`.`permissions` (`id`, `permission_name`, `parent_id`, `level`, `route`, `icon`, `module`, `creator_id`, `permission_mark`, `component`, `redirect`, `keepalive`, `type`, `hidden`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES ('263', 'test', '262', '1-262', '/test/test/curd', '', 'test', '1', 'test', 'test_test', '', '1', '1', '1', '0', '1634613508', '1634613508', '0');
INSERT INTO `数据库名`.`permissions` (`id`, `permission_name`, `parent_id`, `level`, `route`, `icon`, `module`, `creator_id`, `permission_mark`, `component`, `redirect`, `keepalive`, `type`, `hidden`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES ('264', '列表', '263', '1-262-263', '', '', 'test', '1', 'test@index', '', '', '1', '2', '1', '1', '1634613509', '1634613509', '0');
INSERT INTO `数据库名`.`permissions` (`id`, `permission_name`, `parent_id`, `level`, `route`, `icon`, `module`, `creator_id`, `permission_mark`, `component`, `redirect`, `keepalive`, `type`, `hidden`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES ('265', '保存', '263', '1-262-263', '', '', 'test', '1', 'test@save', '', '', '1', '2', '1', '1', '1634613509', '1634613509', '0');
INSERT INTO `数据库名`.`permissions` (`id`, `permission_name`, `parent_id`, `level`, `route`, `icon`, `module`, `creator_id`, `permission_mark`, `component`, `redirect`, `keepalive`, `type`, `hidden`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES ('266', '读取', '263', '1-262-263', '', '', 'test', '1', 'test@read', '', '', '1', '2', '1', '1', '1634613509', '1634613510', '0');
INSERT INTO `数据库名`.`permissions` (`id`, `permission_name`, `parent_id`, `level`, `route`, `icon`, `module`, `creator_id`, `permission_mark`, `component`, `redirect`, `keepalive`, `type`, `hidden`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES ('267', '更新', '263', '1-262-263', '', '', 'test', '1', 'test@update', '', '', '1', '2', '1', '1', '1634613510', '1634613510', '0');
INSERT INTO `数据库名`.`permissions` (`id`, `permission_name`, `parent_id`, `level`, `route`, `icon`, `module`, `creator_id`, `permission_mark`, `component`, `redirect`, `keepalive`, `type`, `hidden`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES ('268', '删除', '263', '1-262-263', '', '', 'test', '1', 'test@delete', '', '', '1', '2', '1', '1', '1634613510', '1634613511', '0');

### 模块roadmap
1. 实现更多可自动生成的控件类型，使生成的页面不仅只管理员可用，还可以提供用户使用。
2. 管理后台增加可复用模板库，开发者可共享模板。
3. 在UCToo应用市场提供模板交易板块和分成机制。

## 扩展开发  
可以参考生成curd.vue的示例，进行更多可复用模板的扩展开发。建议的开发流程：  
1. 先根据需求或根据设计稿，写一个完整可用的vue页面。 
2. 分析出页面可复用的部分以及需要动态替换的内容，抽取出公共部分形成可复用模板文件(参考elmform.stub模板)。 
3. 根据可复用模板文件，写一个动态生成页面的命令行工具（参考CreateElmformCommand.php）。
4. 在控制台执行命令行工具，或者在需要的代码处调用命令行工具，输入用于填充可复用模板的动态数据（一般是json数据），保存生成的页面源码。
5. 手动合并生成的页面源码，也可以在命令行设计时加入输出路径参数，直接将生成的页面源码路径设置为前端项目开发目录（可能要考虑加入检测默认覆盖已有文件的特性）。


  具体请参考开源版开发手册 https://www.kancloud.cn/doc_uctoo/uctoo_dev 及 本开源项目示例  